CREATE TABLE PERSONAGGI (
    ID        INTEGER       PRIMARY KEY AUTOINCREMENT
                            UNIQUE
                            NOT NULL,
    ID_UTENTE INTEGER       NOT NULL,
    NOME      VARCHAR (200) NOT NULL,
    ID_GRUPPO INTEGER
);

INSERT INTO PERSONAGGI (
                           ID_GRUPPO,
                           NOME,
                           ID_UTENTE,
                           ID
                       )
                       VALUES (
                           1,
                           'Leo',
                           2,
                           1
                       );

COMMIT;