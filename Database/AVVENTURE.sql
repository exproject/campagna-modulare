CREATE TABLE AVVENTURE (
    ID        INTEGER        PRIMARY KEY AUTOINCREMENT
                             UNIQUE
                             NOT NULL,
    NOME      VARCHAR (100)  NOT NULL,
    OBIETTIVO VARCHAR (1000) NOT NULL,
    ESITO     NUMERIC        NOT NULL
);

INSERT INTO AVVENTURE (
                          ESITO,
                          OBIETTIVO,
                          NOME,
                          ID
                      )
                      VALUES (
                          0,
                          'Salvare la principessa',
                          'L''inizio dell''avventura',
                          1
                      );

COMMIT;