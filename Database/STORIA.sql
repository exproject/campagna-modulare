CREATE TABLE STORIA (
    ID                      INTEGER PRIMARY KEY AUTOINCREMENT
                                    UNIQUE
                                    NOT NULL,
    ID_AVVENTURA            INTEGER NOT NULL,
    ESITO                   NUMERIC,
    ID_AVVENTURA_SUCCESSIVA INTEGER
);

INSERT INTO STORIA (
                       ID_AVVENTURA_SUCCESSIVA,
                       ESITO,
                       ID_AVVENTURA,
                       ID
                   )
                   VALUES (
                       2,
                       1,
                       1,
                       1
                   ),
                   (
                       3,
-                      1,
                       1,
                       2
                   );

COMMIT;