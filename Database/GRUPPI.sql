CREATE TABLE GRUPPI (
    ID                   INTEGER PRIMARY KEY AUTOINCREMENT
                                 UNIQUE
                                 NOT NULL,
    ID_MASTER            INTEGER NOT NULL,
    ID_CAMPAGNA          INTEGER,
    ID_AVVENTURA_ATTUALE INTEGER
);

INSERT INTO GRUPPI (
                       ID_AVVENTURA_ATTUALE,
                       ID_CAMPAGNA,
                       ID_MASTER,
                       ID
                   )
                   VALUES (
                       1,
                       1,
                       1,
                       1
                   );

COMMIT;