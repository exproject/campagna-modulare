CREATE TABLE CAMPAGNE (
    ID                    INTEGER PRIMARY KEY AUTOINCREMENT
                                  UNIQUE
                                  NOT NULL,
    ID_AVVENTURA_INIZIALE INTEGER NOT NULL
);

INSERT INTO CAMPAGNE (
                         ID_AVVENTURA_INIZIALE,
                         ID
                     )
                     VALUES (
                         1,
                         1
                     );

COMMIT;