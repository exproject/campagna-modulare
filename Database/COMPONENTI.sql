CREATE TABLE COMPONENTI (
    ID    INTEGER      PRIMARY KEY AUTOINCREMENT
                       NOT NULL
                       UNIQUE,
    TIPO  VARCHAR (15) NOT NULL,
    TESTO TEXT (1000) 
);


INSERT INTO COMPONENTI (
                           TESTO,
                           TIPO,
                           ID
                       )
                       VALUES (
                           'Questo è l''inizio dell''avventura, dove viene introdotta la storia, spiegato l''obiettivo e descritta la situazione. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed malesuada aliquam lacus ut luctus. Nam sagittis finibus ultrices. Proin lacus neque, faucibus a odio nec, vulputate ornare enim. Cras enim magna, tempus viverra venenatis a, congue vel enim. Mauris iaculis lectus posuere justo luctus varius. Maecenas id nulla vulputate, blandit metus quis, semper ex. Sed tempor mi id mauris scelerisque tincidunt. Vestibulum auctor risus in diam lobortis, eget volutpat felis lobortis. Quisque ac mauris ultrices turpis tempus pharetra. Cras vitae arcu convallis, luctus elit a, egestas enim. Suspendisse nisl justo, lacinia eget dapibus id, scelerisque id nisi. Donec at varius sapien, ac sodales ipsum.',
                           'TXT',
                           1
                       ),
                       (
                           'Questa è una parte condizionale, in base ad uno dei tag. Integer velit lacus, faucibus eu luctus consectetur, fermentum eget orci. Aliquam erat volutpat. In hac habitasse platea dictumst. Aliquam tortor erat, sagittis non arcu ut, bibendum elementum turpis. Cras non eros eu urna malesuada tristique. Nam aliquet, purus eu interdum imperdiet, quam arcu elementum mi, sit amet sollicitudin neque purus at ex. Vivamus rhoncus, tellus in tempor tincidunt, dui leo gravida ex, et iaculis est lacus at urna. Curabitur vulputate, nulla non facilisis laoreet, risus sem eleifend felis, a iaculis justo urna vel sem.',
                           'TXT',
                           2
                       ),
                       (
                           'Questa è un''altra parte condizionale, in base ad uno dei tag. Morbi ut ex dictum, blandit arcu vel, cursus turpis. Donec pharetra, sem nec tempor blandit, risus orci elementum est, a rutrum ex quam id nibh. Vestibulum pulvinar ornare tortor, vel consequat libero vestibulum a. Donec efficitur neque eget lorem malesuada ultricies. Quisque eu nisi sit amet elit mollis scelerisque. Phasellus porttitor egestas eleifend. Maecenas ullamcorper augue ipsum, mollis dapibus est pulvinar at. Duis non sem tristique, pharetra nunc in, porta libero.',
                           'TXT',
                           3
                       ),
                       (
                           'immagine',
                           'IMG',
                           4
                       );

COMMIT;