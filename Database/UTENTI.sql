CREATE TABLE UTENTI (
    ID          INTEGER       PRIMARY KEY AUTOINCREMENT
                              UNIQUE
                              NOT NULL,
    NOME_UTENTE VARCHAR (200) 
);

INSERT INTO UTENTI (
                       NOME_UTENTE,
                       ID
                   )
                   VALUES (
                       'AndLeo',
                       1
                   );

COMMIT;