CREATE VIEW MODULI AS
    SELECT G.ID_MASTER,
           A.NOME,
           A.OBIETTIVO,
           S.ORDINE,
           C.TESTO
      FROM GRUPPI G,
           AVVENTURE A,
           STRUTTURE S,
           COMPONENTI C
     WHERE A.ID = G.ID_AVVENTURA_ATTUALE AND 
           A.ID = S.ID_AVVENTURA AND 
           S.ID_COMPONENTE = C.ID AND 
           (S.CONDIZIONI IS NULL OR 
            EXISTS (
                SELECT 1
                  FROM TAGS_PER_CAMPAGNA TC,
                       TAGS T
                 WHERE TC.ID_CAMPAGNA = G.ID_CAMPAGNA AND 
                       TC.ID_TAG = T.ID AND 
                       T.NOME = S.CONDIZIONI
            )
           ) 
     ORDER BY S.ORDINE;
