CREATE TABLE STRUTTURE (
    ID            INTEGER       PRIMARY KEY AUTOINCREMENT
                                UNIQUE
                                NOT NULL,
    ID_AVVENTURA  INTEGER       NOT NULL,
    ID_COMPONENTE INTEGER       NOT NULL,
    ORDINE        INTEGER       NOT NULL,
    CONDIZIONI    VARCHAR (100) 
);

INSERT INTO STRUTTURE (
                          CONDIZIONI,
                          ORDINE,
                          ID_COMPONENTE,
                          ID_AVVENTURA,
                          ID
                      )
                      VALUES (
                          NULL,
                          1,
                          1,
                          1,
                          1
                      ),
                      (
                          NULL,
                          3,
                          4,
                          1,
                          2
                      ),
                      (
                          'Y',
                          2,
                          3,
                          1,
                          3
                      ),
                      (
                          'X',
                          2,
                          2,
                          1,
                          4
                      );
COMMIT;