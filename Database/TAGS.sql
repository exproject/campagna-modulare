CREATE TABLE TAGS (
    ID   INTEGER       PRIMARY KEY AUTOINCREMENT
                       UNIQUE
                       NOT NULL,
    NOME VARCHAR (100) UNIQUE
                       NOT NULL,
    TIPO VARCHAR (100) 
);

INSERT INTO TAGS (
                     TIPO,
                     NOME,
                     ID
                 )
                 VALUES (
                     'TRAMA',
                     'Y',
                     1
                 ),
                 (
                     'TRAMA',
                     'X',
                     2
                 );

COMMIT;